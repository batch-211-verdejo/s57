let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
    return collection
}

//Adds an element/elements to the end of the queue
function enqueue(item) {
    // collection.push(item)

    // stretch goal
    collection[collection.length] = item
    return collection
}

//Removes an element in front of the queue
function dequeue(){
    // collection.shift()

    // stretch goal
    for (let i = 1; i < collection.length; i++) 
    
    collection[i - 1] = collection[i];

    collection.length--;

    return collection
    
}


//Shows the element at the front
function front(){
    return collection[0]
}

//Shows the total number of elements
function size(){
    // return collection.length

    // stretch goal
    let length = 0

    for (let i of collection) length++

    return length
}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){
    if (collection.length === 0) {
        return true;
    }
    else {
        return false;
    }
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};